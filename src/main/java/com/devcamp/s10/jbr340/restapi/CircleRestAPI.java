package com.devcamp.s10.jbr340.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CircleRestAPI {
    @CrossOrigin
    @GetMapping("/circle")
    public ArrayList<Double> getRadius() {
        ArrayList<Double> radius = new ArrayList<>();
        Circle circle01 = new Circle(2.0);
        Circle circle02 = new Circle(1.5);

        radius.add(circle01.getArea());
        radius.add(circle01.getChuVi());
        radius.add(circle02.getArea());
        radius.add(circle02.getChuVi());

        return radius;
    }
}
